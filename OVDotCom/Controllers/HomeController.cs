﻿using System.Web.Mvc;
using OVDotCom.Utils;

namespace OVDotCom.Controllers
{
    public class HomeController : Controller
    {
        private IUtils Utils { get; set; }

        public HomeController(IUtils utils)
        {
            Utils = utils;
        }
        
        public ActionResult Index()
        {
            return View();
        }

    }
}
