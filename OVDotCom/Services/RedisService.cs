﻿using System;
using System.Collections.Generic;
using OVDotCom.Interfaces;
using OllieVaughanDotCom.Models;
using ServiceStack.Redis;

namespace OVDotCom.Services
{
    public class RedisService : IBlogService
    {

        //private PooledRedisClientManager _redisManager = new PooledRedisClientManager("192.168.231.151:6379");        
        private readonly PooledRedisClientManager _redisManager;

        public RedisService(PooledRedisClientManager redisManager)
        {
            _redisManager = redisManager;
        }

        /// <summary>
        /// Add a new blog entry to Redis
        /// </summary>
        /// <param name="author">A string containing the author name</param>
        /// <param name="content">A string containing the body of the blog</param>
        /// <param name="category"> </param>
        public void Add(string author, string content, string category)
        {
            _redisManager.ExecAs<BlogModel>(blogs =>
                                               {
                                                   var todo = new BlogModel
                                                                  {
                                                                      Id = blogs.GetNextSequence(),
                                                                      Author = author,
                                                                      Content = content,
                                                                      Date = DateTime.Now,
                                                                      Order = 1,
                                                                  };

                                                   blogs.Store(todo);
                                               });
        }

        /// <summary>
        /// Get an unfiltered list of all blogs in Redis
        /// </summary>
        /// <returns>An infiltered list of all blogs</returns>
        public IList<BlogModel> GetAll()
        {
            return _redisManager.ExecAs<BlogModel>(blogs => blogs.GetAll());
        }

        /// <summary>
        /// Delete a blog by its id
        /// </summary>
        /// <param name="blogId"></param>
        public void Delete(string blogId)
        {
            _redisManager.ExecAs<BlogModel>(blogs => blogs.DeleteById(blogId));
        }

    }
}