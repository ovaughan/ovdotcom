﻿using System.Reflection;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Ninject;
using Ninject.Syntax;
using Ninject.Web.Common;
using OVDotCom.App_Start;
using OVDotCom.Interfaces;
using OVDotCom.Services;
using OVDotCom.Utils;
using ServiceStack.Redis;

namespace OVDotCom
{

    public class MvcApplication : NinjectHttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new
                {
                    controller = "Home",
                    action = "Index",
                    id = UrlParameter.Optional
                });

            routes.MapRoute(
                "Pagination", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new
                {
                    controller = "Pagination",
                    action = "DisplayAll",
                    id = UrlParameter.Optional
                });
        }

        protected override IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());
            RegisterServices(kernel);
            return kernel;
        }


        private static void RegisterServices(IBindingRoot kernel)
        {
            kernel.Bind<IUtils>().To<JsonUtils>();
            kernel.Bind<IBlogService>().To<RedisService>();
            kernel.Bind<IRedisClientsManager>().To<PooledRedisClientManager>().WithConstructorArgument("192.168.231.151:6379");
        }

        protected override void OnApplicationStarted()
        {
            base.OnApplicationStarted();

            AreaRegistration.RegisterAllAreas();
            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}