﻿using System.Collections.Generic;
using OllieVaughanDotCom.Models;

namespace OVDotCom.Interfaces
{
    public interface IBlogService
    {
        void Add(string author, string content, string category);
        IList<BlogModel> GetAll();
        void Delete(string blogId);
    }
}