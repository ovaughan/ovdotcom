﻿using System.IO;
using System.Web;
using Newtonsoft.Json.Converters;

namespace OVDotCom.Utils
{
    public class JsonUtils : IUtils
    {
        /// <summary>
        /// Generate a list of employees using the json file provided
        /// </summary>
        /// <returns>A list of populated employee objects</returns>
        public void Load()
        {
            using (var r = new StreamReader(HttpContext.Current.Server.MapPath("~/TextFiles/Employees.json")))
            {
                const string format = "yyyyMMdd"; // your datetime format
                var dateTimeConverter = new IsoDateTimeConverter { DateTimeFormat = format };

                var json = r.ReadToEnd();
                //return JsonConvert.DeserializeObject<List>(json, dateTimeConverter);
            }
        }
    }
}