﻿using System;

namespace OllieVaughanDotCom.Models
{
    public class BlogModel
    {
        public long Id { get; set; }
        public string Author { get; set; }
        public DateTime Date { get; set; }
        public string Content { get; set; }
        public int Order { get; set; }
        public bool Done { get; set; }

    }
}
